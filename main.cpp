#include <iostream>
#include <iomanip> // std::setprecision
#include <limits>  // std::numeric_limits<long double>, std::numeric_limits<std::streamsize>::max()
#include <cmath>   // std::pow, std::tgamma, std::sin

#define PRECISION 6
#define CELL_SIZE PRECISION+2+3

#define CELL(x) std::setw(CELL_SIZE) << x
#define factorial(x) std::tgamma(x+1)

unsigned getprecision(double fractional)
{
	unsigned precision = 0;
	while (fractional != 0) {
		fractional *= 10;
		fractional -= int(fractional);
		precision ++;
	}
	return precision;
}

int task1()
{
	unsigned n, m;
	unsigned summation = 0;

	std::cout << "n: ";
	std::cin >> n;
	
	std::cout << "m: ";
	std::cin >> m;

	// check correctness of the input
	if (!(m < n))
		return 1;
	
	for (int i = 5; i <= n; i+=5) {
		if (i % m != 0)
			summation += i;
	}

	std::cout << summation << std::endl;
	
	return 0;
}

int task2()
{
	double a;
	std::cout << "a: ";
	std::cin >> a;
	double product = 1;

	if (a >= 0) {
		for (int i = 2; i <= 8; i+=2)
		{
			product *= i * i;
		}
		product -= a;
	} else {
		for (int i = 3; i <= 9; i+=3)
		{
			product *= i - 2;
		}
	}

	double integral;
	double fractional = std::modf(product, &integral);
	
	std::cout << std::setprecision(getprecision(fractional));

	std::cout << std::fixed << product << std::endl;

	return 0;
}

int task3()
{
	const double mina = pow(0.1, PRECISION);

	std::cout << std::fixed << std::left;
	std::cout << std::setprecision(PRECISION);

	// title
	std::cout << CELL("X:") << CELL("S:") << CELL("Y:") << CELL("N:") << std::endl;
	
	for (double x=0; x <= 1; x+=0.2) {
		double a0 =  1 * std::pow(x, 1) / factorial(1);
		double a1 = -1 * std::pow(x, 3) / factorial(3);

		double nexta = a1;
		double s = a0 + a1;

		// n is number of last addendum (n e {0, 1, 2, ...})
		unsigned n = 1;
		while (std::abs(nexta) >= mina) {
			nexta *= -std::pow(x, 2) / (2*n+2) / (2*n+3);
			n ++;
			s += nexta;
		}

		double y = std::sin(x);
		std::cout << CELL(x) << CELL(s) << CELL(y) << CELL(n) << std::endl;
	}

	return 0;
}

int task4()
{
	double y = 0;
	double nmax, x;

	std::cout << "n: ";
	std::cin >> nmax;

	std::cout << "x: ";
	std::cin >> x;

	for (int n = 1; n <= nmax; n++)
	{
		y += std::pow(-1, n) * std::pow(x, 2*n) / factorial(2*n);

		if (n == 3 || n == 5 || n == 10) {
			std::cout << "sum (n=" << n << "): "<< y << std::endl;
		}
	}

	std::cout << "sum (n=" << nmax << "): "<< y << std::endl;
	return 0;
}

int user_cycle(int (*work)())
{
	int result = 1;
	int answer = 'y';
	unsigned length = 0;

	while (answer != 'n')
	{
		// do task
		result = work();

		// clear input buffer (especially after std::cin)
		std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');

		// user choice to continue
		do {
			std::cout << "Продолжить работу? (y/n): ";
			answer = std::getchar();
			length = 1;

			if (answer == EOF) {
				std::cout << std::endl;
				return 0;
			}

			if (answer != '\n')
				for (; std::getchar() != '\n'; length++);

		// break do-while if answer is 'n' or 'y'
		} while (!((answer == 'n' || answer == 'y') && length == 1));
	}

	return result;
}

int main(int argc, char** argv)
{
	int choice;
	std::cout << "choose task (1, 2, 3, 4): ";
	std::cin  >> choice;

	switch (choice)
	{
		case 1:
			return user_cycle(&task1);
		case 2:
			return user_cycle(&task2);
		case 3:
			return task3();
		case 4:
			return user_cycle(&task4);
	}
	return 1;
}