#!/bin/sh

if [ "$1" == "release" ]; then
    
    mkdir -p ./bin/release/
    g++ main.cpp -o ./bin/release/work

else
    if [ "$1" == "debug" ]; then
    
        mkdir -p ./bin/debug/
        g++ main.cpp -g -o ./bin/debug/work

    else
        echo "build.sh: choose mod (release, debug)"
        exit 2
    fi
fi

if [ $? -ne 0 ]; then
    exit 1
else
    echo "Successfully compiled: $1"
fi